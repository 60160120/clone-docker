![Docker-Mongo](https://user-images.githubusercontent.com/42386614/65640698-5e94c100-dfc1-11e9-8c7c-7db8f60727ce.jpg)
# MongoDB e Mongo Express em Docker Compose

Este arquivo tem como intuito explicar como configurar os usuários e senhas do compose de MongoDB e Mongo Express alocado nesse repositório, e teve como referência este [artigo](https://medium.com/@renato.groffe/mongodb-mongo-express-docker-compose-montando-rapidamente-um-ambiente-para-uso-824f25ca6957) do Renato Groffe.

## Explicando um pouco do código:

### Autenticação do usuário do Mongo Express: 

```
ME_CONFIG_BASICAUTH_USERNAME: <<Nome do usuário>>
ME_CONFIG_BASICAUTH_PASSWORD: <<Senha do usuário>>
```

### Autenticação do MongoDB no Mongo Express:

```
ME_CONFIG_MONGODB_ADMINUSERNAME: <<Nome do usuário do MongoDB>>
ME_CONFIG_MONGODB_ADMINPASSWORD: <<Senha do usuário do MongoDB>>
```

### Autenticação do usuário do MongoDB

```
MONGO_INITDB_ROOT_USERNAME: <<Nome do usuário do MongoDB>>
MONGO_INITDB_ROOT_PASSWORD: <<Senha do usuário do MongoDB>>
```

### Configuração do Mongo Express para acessar o MongoDB

```
ME_CONFIG_MONGODB_PORT: 27017 

<<Aqui está configurada a porta padrão do MongoDB>>
```

### Configuração dos volumes

```
volumes:
    - <<Inserir aqui o caminho do volume do seu banco>>

Ex: /$HOME/Documents/Database/Mongo:/data/db
```